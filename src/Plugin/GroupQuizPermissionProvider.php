<?php

namespace Drupal\gquiz\Plugin;

use Drupal\group\Plugin\GroupContentPermissionProvider;

/**
 * Provides group permissions for group_node GroupContent entities.
 */
class GroupQuizPermissionProvider extends GroupContentPermissionProvider {

  /**
   * {@inheritdoc}
   */
  public function getEntityViewUnpublishedPermission($scope = 'any') {
    if ($scope === 'any') {
      // Backwards compatible permission name for 'any' scope.
      return "view unpublished $this->pluginId entity";
    }
    return parent::getEntityViewUnpublishedPermission($scope);
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTakePermission() {
    if ($this->definesEntityPermissions) {
      return "take $this->pluginId";
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission($operation, $target, $scope = 'any') {
    assert(in_array($target, ['relation', 'entity'], TRUE), '$target must be either "relation" or "entity"');
    assert(in_array($scope, ['any', 'own'], TRUE), '$target must be either "relation" or "entity"');

    if ($target === 'relation') {
      switch ($operation) {
        case 'view':
          return $this->getRelationViewPermission($scope);
        case 'update':
          return $this->getRelationUpdatePermission($scope);
        case 'delete':
          return $this->getRelationDeletePermission($scope);
        case 'create':
          return $this->getRelationCreatePermission();
      }
    }
    elseif ($target === 'entity') {
      switch ($operation) {
        case 'view':
          return $this->getEntityViewPermission($scope);
        case 'view unpublished':
          return $this->getEntityViewUnpublishedPermission($scope);
        case 'update':
          return $this->getEntityUpdatePermission($scope);
        case 'delete':
          return $this->getEntityDeletePermission($scope);
        case 'create':
          return $this->getEntityCreatePermission();
        case 'take':
          return $this->getEntityTakePermission();
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPermissions() {
    $permissions = [];

    // Provide permissions for the relation (i.e.: The group content entity).
    $prefix = 'Relation:';
    if ($name = $this->getAdminPermission()) {
      $permissions[$name] = $this->buildPermission("$prefix Administer relations");
      $permissions[$name]['restrict access'] = TRUE;
    }

    if ($name = $this->getRelationViewPermission()) {
      $permissions[$name] = $this->buildPermission("$prefix View any entity relations");
    }
    if ($name = $this->getRelationViewPermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix View own entity relations");
    }
    if ($name = $this->getRelationUpdatePermission()) {
      $permissions[$name] = $this->buildPermission("$prefix Edit any entity relations");
    }
    if ($name = $this->getRelationUpdatePermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix Edit own entity relations");
    }
    if ($name = $this->getRelationDeletePermission()) {
      $permissions[$name] = $this->buildPermission("$prefix Delete any entity relations");
    }
    if ($name = $this->getRelationDeletePermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix Delete own entity relations");
    }

    if ($name = $this->getRelationCreatePermission()) {
      $permissions[$name] = $this->buildPermission(
        "$prefix Add entity relations",
        'Allows you to add an existing %entity_type entity to the group.'
      );
    }

    if ($name = $this->getEntityTakePermission()) {
      $permissions[$name] = $this->buildPermission(
        "Take %entity_type entities",
        'Allows you to take %entity_type entity.'
      );
    }

    // Provide permissions for the actual entity being added to the group.
    $prefix = 'Entity:';
    if ($name = $this->getEntityViewPermission()) {
      $permissions[$name] = $this->buildPermission("$prefix View any %entity_type entities");
    }
    if ($name = $this->getEntityViewPermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix View own %entity_type entities");
    }
    if ($name = $this->getEntityViewUnpublishedPermission()) {
      $permissions[$name] = $this->buildPermission("$prefix View any unpublished %entity_type entities");
    }
    if ($name = $this->getEntityViewUnpublishedPermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix View own unpublished %entity_type entities");
    }
    if ($name = $this->getEntityUpdatePermission()) {
      $permissions[$name] = $this->buildPermission("$prefix Edit any %entity_type entities");
    }
    if ($name = $this->getEntityUpdatePermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix Edit own %entity_type entities");
    }
    if ($name = $this->getEntityDeletePermission()) {
      $permissions[$name] = $this->buildPermission("$prefix Delete any %entity_type entities");
    }
    if ($name = $this->getEntityDeletePermission('own')) {
      $permissions[$name] = $this->buildPermission("$prefix Delete own %entity_type entities");
    }

    if ($name = $this->getEntityCreatePermission()) {
      $permissions[$name] = $this->buildPermission(
        "$prefix Add %entity_type entities",
        'Allows you to create a new %entity_type entity and add it to the group.'
      );
    }

    return $permissions;
  }
}
