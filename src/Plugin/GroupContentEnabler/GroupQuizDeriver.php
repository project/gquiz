<?php

namespace Drupal\gquiz\Plugin\GroupContentEnabler;

use Drupal\quiz\Entity\QuizType;
use Drupal\Component\Plugin\Derivative\DeriverBase;

class GroupQuizDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    foreach (QuizType::loadMultiple() as $name => $quiz_type) {
      $label = $quiz_type->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group quiz (@type)', ['@type' => $label]),
        'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
