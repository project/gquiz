<?php

namespace Drupal\gquiz\Plugin;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Plugin\GroupContentAccessControlHandler;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides group permissions for group_node GroupContent entities.
 */
class GroupQuizAccessControlHandler extends GroupContentAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function entityAccess(EntityInterface $entity, $operation, AccountInterface $account, $return_as_object = FALSE) {
   // return AccessResultAllowed::allowed();


    /** @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('group_content');
    $group_contents = $storage->loadByEntity($entity);

    // Filter out the content that does not use this plugin.
    foreach ($group_contents as $id => $group_content) {
      // @todo Shows the need for a plugin ID base field.
      $plugin_id = $group_content->getContentPlugin()->getPluginId();
      if ($plugin_id !== $this->pluginId) {
        unset($group_contents[$id]);
      }
    }

    // If this plugin is not being used by the entity, we have nothing to say.
    if (empty($group_contents)) {
      return AccessResult::neutral();
    }

    // We only check unpublished vs published for "view" right now. If we ever
    // start supporting other operations, we need to remove the "view" check.
    $check_published = FALSE;
    if (($operation == 'view' || $operation == 'take') && $entity->getEntityType()->entityClassImplements(EntityPublishedInterface::class)) {
      $check_published = TRUE;
    }

    // Check if the account is the owner and an owner permission is supported.
    $is_owner = FALSE;
    if ($entity->getEntityType()->entityClassImplements(EntityOwnerInterface::class)) {
      $is_owner = $entity->getOwnerId() === $account->id();
    }

    // Add in the admin permission and filter out the unsupported permissions.
    $permissions = [$this->permissionProvider->getAdminPermission()];
    if (!$check_published || $entity->isPublished()) {
      $permissions[] = $this->permissionProvider->getPermission($operation, 'entity', 'any');
      $own_permission = $this->permissionProvider->getPermission($operation, 'entity', 'own');
      if ($is_owner) {
        $permissions[] = $own_permission;
      }
    }
    elseif ($check_published && !$entity->isPublished()) {
      $permissions[] = $this->permissionProvider->getPermission("$operation unpublished", 'entity', 'any');
      $own_permission = $this->permissionProvider->getPermission("$operation unpublished", 'entity', 'own');
      if ($is_owner) {
        $permissions[] = $own_permission;
      }
    }

    $permissions = array_filter($permissions);

    foreach ($group_contents as $group_content) {
      $result = GroupAccessResult::allowedIfHasGroupPermissions($group_content->getGroup(), $account, $permissions, 'OR');
      if ($result->isAllowed()) {
        break;
      }
    }

    // If we did not allow access, we need to explicitly forbid access to avoid
    // other modules from granting access where Group promised the entity would
    // be inaccessible.
    if (!$result->isAllowed()) {
      $result = AccessResult::forbidden()->addCacheContexts(['user.group_permissions']);
    }

    // If there was an owner permission to check, the result needs to vary per
    // user. We also need to add the entity as a dependency because if its owner
    // changes, someone might suddenly gain or lose access.
    if (!empty($own_permission)) {
      // @todo Not necessary if admin, could boost performance here.
      $result->cachePerUser();
    }

    // If we needed to check for the owner permission or published access, we
    // need to add the entity as a dependency because the owner or publication
    // status might change.
    if (!empty($own_permission) || $check_published) {
      // @todo Not necessary if admin, could boost performance here.
      $result->addCacheableDependency($entity);
    }

    return $return_as_object ? $result : $result->isAllowed();
  }
}
