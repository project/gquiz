Group Quiz

Integrate the Quiz (https://www.drupal.org/project/quiz) module with Group (https://www.drupal.org/project/group) module.

Drupal 9+
Module working correctly only with 8.x-1.x version of the module Group.

Supporting organizations:
Diya (https://diya.pro)
